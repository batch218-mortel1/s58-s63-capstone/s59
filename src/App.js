import { useState, useEffect } from 'react';

import {UserProvider} from './UserContext';

import {Container} from 'react-bootstrap';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import './App.css';

import AppNavbar from './components/AppNavbar';
// import CourseView from './components/CourseView';


// import Home from './pages/Home';
// import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
// import Logout from './pages/Logout';
// import ErrorPage from './pages/ErrorPage'

function App() {

  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {

          if(typeof data._id !== "undefined") {

              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          } 

          else { 
              setUser({
                  id: null,
                  isAdmin: null
              })
          }

      })
  }, []);


  return (

    <>
    <UserProvider value={{user, setUser, unsetUser}}>

    <Router>
      <AppNavbar />
      <Container>
          <Routes> 
            <Route path= "/" element={<Home />} />
            // <Route path= "/courses" element={<Courses />} />
            // <Route path= "/courses/:courseId" element={<CourseView />} />
            <Route path= "/register" element={<Register />} />
            <Route path= "/login" element={<Login />} />
            // <Route path= "/logout" element={<Logout />} />
            // {
            //   "*" - Wildcard character that will match any path that was defined in routes
            // }   
            // <Route path="/*" element={<ErrorPage/>}/>
          </Routes>
      </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;