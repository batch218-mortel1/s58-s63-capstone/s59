import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({course}) {

    const { name, description, price, _id } = course;

    function enroll() {

    return (
    <Card>
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PhP {price}</Card.Text>
            <Button className="bg-primary" as={Link} to={`/products/${_id}`} >Details</Button>
        </Card.Body>
    </Card>
    )
};

// "proptypes" - are a good way of checking data type of information between components.
CourseCard.propTypes = {
    // "shape" method is used to check if prop object conforms to a specific "shape"
    course: PropTypes.shape({
        // Defined properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
